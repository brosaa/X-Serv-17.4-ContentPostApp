#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp

class contentPostApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""

        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\r\n\r\n', 1)[1]
        return method, resource, body

    def process(self, resourcename):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        (method, resource, body) = resourcename

        if method == "POST":
            resource = '/' + body.split('=')[1]
            self.content[resource] = body.split("=")[1] #Añadir al diccionario content

        if resource in self.content.keys():
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.content[resource]
        else:
            httpCode = "404 Not Found"
            htmlBody = "<html><body>Not Found"

        htmlBody += """ <form action="" method = "POST"> 
             <input type="text" name="name" value="">
             <input type="submit" value="Enviar"> </form> """ + "</body></html>"

        return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = contentPostApp("localhost", 1234)
